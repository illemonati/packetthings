package main

import (
	"fmt"
	"log"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)

const (
	device  string        = "wlan0"
	snaplen int32         = 65535
	promisc bool          = false
	timout  time.Duration = -1 * time.Second
	ip      string        = "192.168.0.63"
)

func main() {
	handle, err := pcap.OpenLive(device, snaplen, promisc, timout)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	filter := "icmp"
	err = handle.SetBPFFilter(filter)
	if err != nil {
		log.Fatal(err)
	}

	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	for packet := range packetSource.Packets() {
		go ProcessICMP(packet, handle)
	}
}

// ProcessICMP : Function to process ICMP
func ProcessICMP(packet gopacket.Packet, handle *pcap.Handle) {
	ethernetLayer := packet.Layer(layers.LayerTypeEthernet)
	ethernetFrame, ok := ethernetLayer.(*layers.Ethernet)
	ipv4Layer := packet.Layer(layers.LayerTypeIPv4)
	ipPacket := ipv4Layer.(*layers.IPv4)
	icmpv4Layer := packet.Layer(layers.LayerTypeICMPv4)
	icmpPacket := icmpv4Layer.(*layers.ICMPv4)
	if ok {
		if icmpPacket.TypeCode.Type() == layers.ICMPv4TypeEchoRequest {
			reply(ethernetFrame, ipPacket, icmpPacket, handle)
		}
	}
	fmt.Println("------------------------------------------------------------------")
	fmt.Println("______________________________")
	fmt.Println("Source : " + ipPacket.SrcIP.String())
	fmt.Println("Destination : " + ipPacket.DstIP.String())
	fmt.Println("Protocol : " + ipPacket.Protocol.String())
	fmt.Println("______________________________")
	fmt.Println("ICMP Code : " + icmpPacket.TypeCode.String())
	fmt.Println("ICMP Sequence Number : " + string(icmpPacket.Seq))
	fmt.Println("Payload data length : " + string(len(icmpPacket.Payload)))
	fmt.Println("Payload data", icmpPacket.Payload)
	fmt.Println("Payload data as string : ", string(icmpPacket.Payload))
	fmt.Println("\n------------------------------------------------------------------\n\n")
}

func reply(ethernetFrame *layers.Ethernet, ipPacket *layers.IPv4, icmpPacket *layers.ICMPv4, handle *pcap.Handle) {
	defer fmt.Println("Replyed")
	EFReversed := ethernetFrame
	t := ethernetFrame.SrcMAC
	EFReversed.SrcMAC = ethernetFrame.DstMAC
	EFReversed.DstMAC = t

	IPReversed := ipPacket
	ti := ipPacket.SrcIP
	IPReversed.SrcIP = ipPacket.DstIP
	IPReversed.DstIP = ti

	ICMPReversed := icmpPacket
	ICMPReversed.TypeCode = layers.ICMPv4TypeEchoReply

	buffer := gopacket.NewSerializeBuffer()
	var options gopacket.SerializeOptions
	options.ComputeChecksums = true

	payload := append(ICMPReversed.Payload, []byte("You're ultra special!!! Please Don't hack my computer! :)")...)
	payload = append(payload, ICMPReversed.Payload...)

	gopacket.SerializeLayers(
		buffer,
		options,
		EFReversed,
		IPReversed,
		ICMPReversed,
		gopacket.Payload(payload),
	)

	message := buffer.Bytes()
	err := handle.WritePacketData(message)
	if err != nil {
		log.Fatal(err)
	}
}
